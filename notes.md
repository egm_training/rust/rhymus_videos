# Rhymus rust tutorial

## Part 1 : install and run

### Install rust

Use `rustup`

```shell
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

**update rust**

```rust
rustup update
```

### Make your first rust program

```rust
cargo new <name>
```

> Cargo is the package manager, it deals with packages and creates

**Package** : is a collection of creates
**Create** : program, library, app ...

- binary
- app

Cargo creates a `toml` file. Its a manifest, metadata about the package, dependencies, version, name, edition...

By default Rust creates a git repo, but if a repo is already created, it does nothing. we need to add `target` to the ignore file.

### Run a program

```rust
cargo new <name>
```

## Part 2 : Functions and module

### Functions

`main`: is the principal function, it is the reserved word.

Functions are classical but does not need to be defined before calling.

### Modules

To organize functions we can use `modules`

- Functions in modules are private by default, we need to add `pub` before the function name
  ```rust
  mod printing {
    // Methods are private by default so add pub
    pub fn say_goodbye() {
    println!("Goodbye!");
    }
  }
  ```
- We need to add `name_module::function` to acces the function or use an alias

**Multilevel modules**

We can add nested modules like:

```rust
mod printing {
    // Methods are private by default  so  add pub
    pub fn say_goodbye() {
        println!("Goodbye!");
    }

    // This module needs to be public
    pub mod time_stuff {
        // A function to give the time
        pub fn print_hour() {
            println!("The current time is {}", chrono::Local::now());
        }
    }
}
```

To acces the function now we have:

```rust
fn main() {
    printing::time_stuff::print_hour();
}
```

Or we can use an alias

```rust
mod printing {
//...
  pub use time_stuff::print_hour as announce_time;
// ...
}

fn main() {
    ptm::announce_time();
}
```

Finally we can put the modules on different files

```rust
// We define a new module
mod printing {
    // Methods are private by default  so  add pub
    pub fn say_goodbye() {
        println!("Goodbye!");
    }

    // This module needs to be public
    // Rust will look the module on the directory printing/time_stuff.rs
    pub mod time_stuff;

    // add an alias
    pub use time_stuff::print_hour as announce_time;
}
```

and create a file on :

```shell
├── src
│   ├── main.rs
│   └── printing
│       └── time_stuff.rs
```

We can generalize this ideas to separate the module printing from the main file.

```shell
├── src
│   ├── main.rs
│   ├── printing
│   │   └── time_stuff.rs
│   └── printing.rs
```

The `main.rs` :

```rust
// Import module
mod printing;
// Using alias
use printing as ptm;

// Main function
fn main() {

    println!("Hello, world!");
    ptm::say_goodbye();
    ptm::announce_time();
}
```

and the `printing.rs file`

```rust
// This is module printing

// Functions of module printing
// Methods are private by default  so  add pub
pub fn say_goodbye() {
    println!("Goodbye!");
}

// Add a submodule
// Rust will look the module on the directory printing/time_stuff.rs
pub mod time_stuff;
// add an alias
pub use time_stuff::print_hour as announce_time;
```

### External creates

Using external creates.

We need to import them, using the `cargo.toml` file

```toml
[dependencies]
# adding an external crate
chrono = "0.4.19"
```

## Tools

## Extensions

- [rust-analyser] : syntax, debug, autocomplete, format
- [crates] : helps with last version create

<!-- Links -->

[rust-analyser]: https://marketplace.visualstudio.com/items?itemName=matklad.rust-analyzer
[crates]: https://marketplace.visualstudio.com/items?itemName=serayuzgur.crates
