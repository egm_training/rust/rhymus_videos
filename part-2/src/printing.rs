// This is module printing 

// Functions of module printing 
// Methods are private by default  so  add pub
pub fn say_goodbye() {
    println!("Goodbye!");
}

// Add a submodule
// Rust will look the module on the directory printing/time_stuff.rs
pub mod time_stuff;
// add an alias
pub use time_stuff::print_hour as announce_time;
