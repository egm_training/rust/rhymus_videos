// This is the submodule time_stuff of printing

// Functions of submodule time_stuff
// A function to give the time
pub fn print_hour() {
    println!("The current time is {}", chrono::Local::now());
}
