// Import module
mod printing;
// Using alias
use printing as ptm;

// Main function
fn main() {
    
    println!("Hello, world!");
    ptm::say_goodbye();
    ptm::announce_time();
}

