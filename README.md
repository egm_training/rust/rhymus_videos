# Introduction to Rust by Rhymus 

This repo contains the [Rhymus](https://www.youtube.com/channel/UC4ZfpU7QX3iSatYB2GDum5Q) tutorials on Rust. 

The playlist is [here](https://youtube.com/playlist?list=PLbtjxiXev6lpd331MW2dB7UgSIovgv169).

```mermaid
gantt
    title Release videos
    dateFormat  YYYY-MM-DD
    section Section
    Part 1 Getting Started: 2021-11-06, 1d
    Part 2 Functions and Modules: 2021-11-13, 1d
    Part 3 Data Types: 2021-11-20, 1d
```

Check my [personal notes](./notes.md)